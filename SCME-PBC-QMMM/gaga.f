c-----------------------------------------------------------------------
c                  New potential based on multipole moments.
c     Each molecule is represented as a multipole expansion up to
c     hexadecapole moment.
c-----------------------------------------------------------------------
c     An arrays with positions enters as argument ra(). 
c     It is assumed that the array of positions ra() has a length equal
c     to 9 times the number of molecules and that its structure is the
c     following: 
c           ra(l+6*(i-1)) stores the l-th coordinate of the first
c                         hydrogen in the i-th molecule
c           ra(l+3+6*(i-1)) stores the l-th coordinate of the second
c                         hydrogen in the i-th molecule
c           ra(l+3*(i-1+nHydrogens)) stores the l-th coordinate of the
c                         oxygen in the i-th molecule. (nHydrogens is
c                         the total number of hydrogen atoms)
c     The routine will return an array fa() with the forces on each atom.
c     The array fa() has the same structure as ra().
c     The total potential energy of the configuration is also calculated
c     and returned in 'uTot'.
c
c     This routine was written by Enrique Batista.
c-----------------------------------------------------------------------

c23456789012345678901234567890123456789012345678901234567890123456789012
c        10        20        30        40        50        60        70

      subroutine gagafe(nAtms, raOri, itagl, fa, uTot, 
     $ virial,convcrit,inscf,QMMM,NQ,ETOUT,
     $ eQM,dEQM,ddEQM,dddEQM,ddddEQM,
     $ DDIN,DQIN,
     $ natm,DIPOLEOUT,QPOLEOUT,OPOLEOUT,HPOLEOUT,
     $ fCMout, DISPOUT, REPOUT, EDISP, EREP, tauout,
     $ DIPQMOUT,QPOLEQMOUT, DDOUT, DQOUT)

c      implicit real*8 (a-h,o-z)


      implicit none
      integer natm
      include '../commonblks/parameters.cmn'
      include '../commonblks/compotent.cmn'
      include '../commonblks/comtime.cmn'
      include '../commonblks/comgeom.cmn'    

ctimming
      integer*8 ti, tf, irtc
      real*8 t1, t2, t3, t4, t5, t6, t7, rMax, rMax2
ctimming
      real*8 d1, d2
      real*8 EDISP, EREP

      real*8 pi, raOri(maxCoo), a(3), a2(3), fa(maxCoo)
      real*8 fCM(3,maxCoo/3), fsf(3,maxCoo/3), tau(3,maxCoo/3) 
      real*8 tauQM(3,maxCoo/3)
      real*8 uD, uQ, uH, uPol, uDisp, uRho, uCore, uES
      real*8 uDip, uMut, uQuad

c     atomic position, centers of mass, principal axes.
      real*8 ra(maxCoo), rCM(3,maxCoo/3), x(3,3,maxCoo/3)

      real*8 convcrit
      intent(in) convcrit
      logical*1 inscf
      intent(in) inscf
      logical*1 QMMM
      intent(in) QMMM
      integer NQ
      intent(in) NQ

c     Electric fields
      real*8 eD(3,maxCoo/3), eQ(3,maxCoo/3), eH(3,maxCoo/3) 
      real*8 eT(3,maxCoo/3)
      real*8 eTQM(3,maxCoo/3)
c     QM field from gpaw edens in from ASE
      real*8 eQM
      intent(in) eQM
      real*8 dEQM
      intent(in) dEQM
      real*8 ddEQM
      intent(in) ddEQM
      real*8 dddEQM
      intent(in) dddEQM
      real*8 ddddEQM
      intent(in) ddddEQM
c     Induced dipole and quadrupole from previous step
      real*8 DDIN(3,maxCoo/3)
      intent(in) DDIN
      real*8 DQIN(3,3,maxCoo/3)
      intent(in) DQIN
                
c     Derivatives of E
      real*8 dEddr(3,3,maxCoo/3), dEqdr(3,3,maxCoo/3)
      real*8 dEhdr(3,3,maxCoo/3), dEtdr(3,3,maxCoo/3)
      real*8 dEtdrQM(3,3,maxCoo/3)

c     High order derivatives of the potential
      real*8 d1v(3,maxCoo/3), d2v(3,3,maxCoo/3), d3v(3,3,3,maxCoo/3)
      real*8 d4v(3,3,3,3,maxCoo/3), d5v(3,3,3,3,3,maxCoo/3) 
c     For induced quadrupole components
      real*8 d1q(3,maxCoo/3), d2q(3,3,maxCoo/3), d3q(3,3,3,maxCoo/3)
      real*8 d4q(3,3,3,3,maxCoo/3), d5q(3,3,3,3,3,maxCoo/3) 

      real*8 uTot, faB(maxCoo), u, virial, convFactor
      real*8 uSS, uIS, uII, uCorr
      real*8 uCov, uAng, Amp, rMin, lambda, gam, rOHmax
      integer nM, nO, nH, nAtms(maxComp), i, ii, j, k, l, NC, nst, Np

c     Unpolarized multipoles
      real*8 d0(3), q0(3,3), o0(3,3,3), h0(3,3,3,3)

c     Work multipoles. They start unpolarized and with the induction
c     loop we induce dipoles and quadrupoles.
      real*8 dpole(3,maxCoo/3), qpole(3,3,maxCoo/3)
      real*8 opole(3,3,3,maxCoo/3), hpole(3,3,3,3,maxCoo/3)

      real*8 dpole0(3,maxCoo/3), qpole0(3,3,maxCoo/3)
      real*8 dpolehalf(3,maxCoo/3)
      real*8 qpolehalf(3,3,maxCoo/3)

c     for the purely QM field induced poles 
      real*8 dPoleQM(3,maxCoo/3), qPoleQM(3,3,maxCoo/3)

c     for other
      real*8 dpoleQMtot(3, maxCoo/3), qpoleQMtot(3,3,maxCoo/3)

c     Polarizabilities
      real*8 dd0(3,3), dq0(3,3,3), hp0(3,3,3), qq0(3,3,3,3)
      real*8 dd(3,3,maxCoo/3), dq(3,3,3,maxCoo/3), hp(3,3,3,maxCoo/3)
      real*8 qq(3,3,3,3,maxCoo/3), dqinv(3,3,3,maxCoo/3)

c     Move along tetrahedral axis
      integer oi
      real*8 m(3,3), mI(3,3), ft(3), dft(3), df(3), ftddr, dot, ddd

c     Study bulk modulus
      real*8 raOld(maxCoo), scale, dr(3)
      integer iO, iH1, iH2
c     Check derivatives
      integer at, coord, iMol, iComp
      real*8 r1(3), r2(3), ro(3)
      real*8 dx, di(3), fexa, fnum, uOld, r0(3)

      integer itagl(maxatoms)
      logical*1 firstVisit, converged, debug, iSlab
      logical*1 addCore

C     Out to ASE
      real*8 ETOUT(3,nAtms(2))
      real*8 DIPOLEOUT(3,nAtms(2))
      real*8 DIPQMOUT(3,nAtms(2))
      integer ct
      intent(out) ETOUT
      intent(out) DIPOLEOUT
      real*8 QPOLEOUT(3,3,nAtms(2))
      real*8 QPOLEQMOUT(3,3,nAtms(2))
      real*8 OPOLEOUT(3,3,3,nAtms(2))
      real*8 HPOLEOUT(3,3,3,3,nAtms(2))
      intent(out) QPOLEOUT
      intent(out) OPOLEOUT
      intent(out) HPOLEOUT
      real*8 fCMout(3,nAtms(2))
      real*8 tauout(3,nAtms(2))
C     Induction parameters
      real*8 DDOUT(3,nAtms(2))
      real*8 DQOUT(3,3,nAtms(2))
      intent(out) DDOUT
      intent(out) DQOUT

      real*8 DISPOUT(*)
      intent(out) DISPOUT
      real*8 REPOUT(*)
      intent(out) REPOUT

C     Debug
      integer     p, q, r, s
      real*8      kk1, kk2
      character*3 Axis
      real*8 weirdcheck(nAtms(1)+nAtms(2),3) 

      data firstVisit / .true. /

      save

      debug = .false.
      if (firstVisit) then
         pi = 3.14159265358979312d0

c     Size of the simulation cell
         a(1) = ax
         a(2) = ay
         a(3) = az
         a2(1) = ax/2
         a2(2) = ay/2
         a2(3) = az/2

         nO = nAtms(2)          ! Number of OXYGENS 
         nH = nAtms(1)          ! Number of HYDROGENS  
         nM = nO                ! Number of molecules
         call readPoles(d0, q0, o0, h0)
         call readPolariz(dd0, dq0, hp0, qq0)

         NC = potpar(2,1)       ! Number of cell in each direction

         rMax = 1000.d0
         rMax2 = rMax*rMax
c     Convert from Debye^2/angstrom^3 to eV
         convFactor = 14.39975841d0 / 4.803206799d0**2
         
         iSlab = .TRUE.

         addCore = .TRUE.

      end if
      NC = NQ
      !print *, NC

      uTot = 0.0d0

c     Recover broken molecules due to PBC
      call recoverMolecules(raOri, ra, nH, nO, a, a2)

      call calcCentersOfMass(ra, nM, rCM)

      call findPpalAxes(ra, nM, x)

      call rotatePoles(d0, q0, o0, h0, dpole0, qpole0, opole, hpole, nM
     $     ,x) 
      call setUnpolPoles(dpole, qpole, dpole0, qpole0, nM) 
      call rotatePolariz(dd0, dq0, qq0, hp0, dd, dq, qq, hp, nM, x)

      call inversedq(dqinv, dq, nM)

C Added by Fer.
      if ( firstVisit ) then

        Axis = 'xyz'
        kk1 = 2.5417709D0
        kk2 = 1.88972666351031921149D0

      end if

      call calcEhigh(rCM, opole, hpole, nM, NC, a, a2, uH, eH, dEhdr,
     $     rMax2, iSlab)

c    add previous induced moments if any, and get out octupole
c    and hexadecapole
      do ct = 1,nAtms(2)
        do i = 1,3
          dpole(i,ct) = dpole(i,ct) + DDIN(i,ct)
          do j = 1,3
           qpole(i,j,ct) = qpole(i,j,ct) + DQIN(i,j,ct)
           do k = 1,3
             OPOLEOUT(i,j,k,ct) = opole(i,j,k,ct)
             do l = 1,3
               HPOLEOUT(i,j,k,l,ct) = hpole(i,j,k,l,ct)
             end do
           end do
          end do
        end do
      end do

c     add previous induced dipole and quadrupole, if any
      

c     Here's where the induction loop begins
      converged = .false.
      do while (.not. converged)
c         ii = ii + 1
         call calcEdip_quad(rCM, dpole, qpole, nM, NC, a, a2, uD, uQ, eD
     $        , dEddr, rMax2, iSlab)

         call addFields(eH, eD, eT, nM)
         call addFieldsQM(eH, eD, eTQM, eQM, nM) 
c        FOR QM/MM embedding via dpole
         call addDfields(dEhdr, dEddr, dEtdr, nM)
         call addDfieldsQM(dEhdr, dEddr, dEtdr, dEtdrQM, dEQM, nM)
c        Induce dipoles and quadrupoles - qpoles QM INTERFACED
         converged = .true.
         call induceDipole(dPole, dpole0, eTQM, dEtdrQM, dd, dq, hp, nM
     $        ,convcrit, converged)
         call induceQpole(qPole, qpole0, eTQM, dEtdrQM, dq, qq, nM,
     $        convcrit, converged) 
      end do                    !End of the induction loop

      call induceDipoleQM(dPoleQM, dpole0, eQM, dEQM, dd, dq, hp, nM
     $        ,convcrit, converged)
      call induceQpoleQM(qPoleQM, qpole0, eQM, dEQM, dq, qq, nM,
     $        convcrit, converged)
c    get out eT(Why?) dipoles and qpoles for ase - and now also only QM induced poles
      do ct = 1,nAtms(2)
        do i = 1,3
          ETOUT(i,ct) = eT(i,ct)
          DIPOLEOUT(i,ct) = dpole(i,ct) 
          DIPQMOUT(i,ct) = dPoleQM(i,ct)
          DDOUT(i,ct) = dpole(i,ct) - dpole0(i,ct)
          dpolehalf(i,ct) = dpole(i,ct) - 0.5d0 * dPoleQM(i,ct)
          do j = 1,3
            QPOLEOUT(i,j,ct) = qpole(i,j,ct)
            DQOUT(i,j,ct) = qpole(i,j,ct) - qpole0(i,j,ct)
            QPOLEQMOUT(i,j,ct) = qPoleQM(i,j,ct)
            qpolehalf(i,j,ct) = qpole(i,j,ct) - 0.5d0 * qPoleQM(i,j,ct)
          end do
        end do
      end do
      
      if (inscf) goto 666

c     With the polarized multipoles, calculate the derivarives of the
c     electrostatic potential, up to 5th order.
      call calcDv(rCM, dpole, qpole, opole, hpole, nM, NC, a, a2, 
     $     d1v, d2v, d3v, d4v, d5v, rMax2, fsf, iSlab)  

c     (mu + dmu)V^MM
c     Compute the force on the center of mass - due to MM fields
      call forceCM(dpole, qpole, opole, hpole, d2v, d3v, d4v,
     $     d5v, nM, fsf, fCM)

c     (mu + 1/2 dmu)V^QM
c     Compute the force on the center of mass - due to QM fields
      call forceQM(dpolehalf, qpolehalf, opole, hpole, deQM, ddeQM, 
     $     dddeQM, ddddeQM, nM, fCM)

c      call torqueCM(dpolehalf, qpolehalf, opole, hpole, deQM, ddeQM,
c     $  dddeQM, ddddeQM, nM, tauQM)

c     Collect fields before evaluating total energy
      call addQMFields(d1v, d2v, d3v, d4v, d5v, 
     $     eQM, deQM, ddeQM, dddeQM, ddddeQM, nM)

c     Compute the torque on the molecule
      call torqueCM(dpole, qpole, opole, hpole, d1v, d2v, d3v,
     $ d4v, nM,
     $     tau)

c     Asmus: I want tau out into ase as well.       
      do ct = 1,nAtms(2)
        do i = 1,3
c          tau(i,ct) = tau(i,ct) - tauQM(i,ct)
          tauout(i,ct) = tau(i,ct)
        end do
      end do

c     Find 3 forces, one at oxygen and one at each hydrogen such that
c     the total force and total torque agree with the ones calculated
c     for the multipoles.

      call atomicForces(fCM, tau, ra, rCM, nM, fa)

      do ct = 1,nAtms(2)
        do i = 1,3
          fCMout(i,ct) = fCM(i,ct)
        end do
      end do

c     Calculate electrostatic interaction energy (includes 1/2 QM)
      call calcEnergy(dpole, qpole, opole, hpole, d1v, d2v, d3v, d4v
     $     , nM, uSS)

c     Self-energy of the MM part
      call polarizationEnergy(dd, dq, qq, hp, d1v, d2v, nM, uPol,
     $ uDip, uMut, uQuad)

c      print *,uPol * convFactor

      uTot = uSS + uPol
    
      do i = 1, 3*(nH+nO)
         fa(i) = convFactor * fa(i)
      end do
      uTot = uTot * convFactor 

      uES = uTot

      call dispersion(ra, fa, uDisp, nM, a, a2, DISPOUT)
      uTot = uTot + uDisp

      EDISP = uDisp 

c      Skip repulsion calculations if this is a QM/MM calculation
      if (QMMM) goto 666
      if (addCore) then
 111     call coreInt(ra, fa, uCore, nM, a, a2, REPOUT)
         uTot = uTot + uCore
         EREP = uCore 
      end if
  
 666  return
      end

