c----------------------------------------------------------------------+
c     Calculate derivative of the potential at the QM coms             |
c----------------------------------------------------------------------+
c      subroutine calcDvQM(rQM, rCM, dpole, qpole, opole, hpole, nQM, nM,
c     $     NC, a, a2, d1v, d2v, d3v, d4v, d5v, rMax2, fsf, iSlab)
c
c      implicit none
c      include '../commonblks/parameters.cmn'
c      integer nQM, nM, NC, NCz
c      real*8 rQM(3,nQM), rCM(3,maxCoo/3), a(3), a2(3)
c      real*8 dpole(3,maxCoo/2), qpole(3,3,maxCoo/3)
c      real*8 opole(3,3,3,maxCoo/3), hpole(3,3,3,3,maxCoo/3)
c
c      real*8 d1v(3,maxCoo/3), d2v(3,3,maxCoo/3), d3v(3,3,3,maxCoo/3)
c      real*8 d4v(3,3,3,maxCoo/3), d5v(3,3,3,3,maxCoo/3), rMax2
c
c      real*8 d1d(3), d2d(3,3), d3d(3,3,3), d4d(3,3,3,3), d5d(3,3,3,3,3)
c      real*8 d1a(3), d2a(3,3), d3a(3,3,3), d4a(3,3,3,3), d5a(3,3,3,3,3)
c
c      real*8 d(3), q(3,3), o(3,3,3), h(3,3,3,3)
c
c      integer i, j, k, l, s, n, m, ii, nx, ny, nz
c      integer in2(2), in3(3), in4(4), in5(5)
c      real*8 re(3), dr(3), r1, r2, swFunc, dSdr
c      logical*1 iSlab
c
c      do n = 1, nQM
c         do i = 1, 3
c            d1v(i,n) = 0.d0
c            do j = i, 3
c               d2v(i,j,n) = 0.d0
c               do k = j, 3
c                  d3v(i,j,k,n) = 0.d0
c                  do l = k, 3
c                     d4v(i,j,k,l,n) = 0.d0
c                     do s = l, 3
c                        d5v(i,j,k,l,s,n) = 0.d0
c                     end do
c                  end do
c               end do
c            end do
c         end do
c      end do
c
c      NCz = NC
c      if (iSlab) NCz = 0
c
c      do n = 1, nQM
c         do m = 1, nM
c
c            do nx = -NC, NC
c               re(1) = a(1) * nx
c               do ny = -NC, NC
c                  re(2) = a(2) * ny
c                  do nz = -NCz, NCz
c                     re(3) = a(3) * nz
c
c                     do i = 1, 3
c                        dr(i) = rQM(i,n) - rCM(i,m)
c                        if (dr(i) .gt. a2(i)) then
c                           dr(i) = dr(i) - a(i)
c                        else if (dr(i) .lt. -a2(i)) then
c                           dr(i) = dr(i) + a(i)
c                        end if
c                        dr(i) = dr(i) + re(i)
c                     end do
c
c                     r2 = dr(1)**2 + dr(2)**2 + dr(3)**2
c
c                     if (r2 .gt. rMax2) goto 11
c                     r1 = sqrt(r2)
c                     call SFdsf(r1, swFunc, dSdr)
c
c                     do i = 1, 3
c                        d(i) = dpole(i,m)
c                     end do
c                     call dDpole(d, dr, d1a, d2a, d3a, d4a, d5a)
c
c                     do j = 1, 3
c                        do i = 1, 3
c                           q(i,j) = qpole(i,j,m)
c                        end do
c                     end do
c                     call dQpole(q, dr, d1d, d2d, d3d, d4d, d5d)
c                     call addDerivA(d1a, d2a, d3a, d4a, d5a, d1d, d2d, 
c     $                    d3d, d4d, d5d)
c
c                     do k = 1, 3
c                        do j = 1, 3
c                           do i = 1, 3
c                              o(i,j,k) = opole(i,j,k,m)
c                           end do
c                        end do
c                     end do


c----------------------------------------------------------------------+
c     Calculate derivative of the potential... not electric field      |
c----------------------------------------------------------------------+
      subroutine calcDvInd(rCM, dpole, dpole0, qpole, qpole0, nM, NC, a
     $ , a2, d1v, d2v, d3v, d4v, d5v, d1qv, d2qv, d3qv, d4qv, d5qv,
     $ rMax2, fsf, iSlab)

      implicit none
      include '../commonblks/parameters.cmn'
      integer nM, NC, NCz

      real*8 rCM(3,maxCoo/3), a(3), a2(3)
      real*8 dpole(3,maxCoo/3), qpole(3,3,maxCoo/3)
      real*8 dpole0(3,maxCoo/3), qpole0(3,3,maxCoo/3)

      real*8 d1v(3,maxCoo/3), d2v(3,3,maxCoo/3), d3v(3,3,3,maxCoo/3)
      real*8 d4v(3,3,3,3,maxCoo/3), d5v(3,3,3,3,3,maxCoo/3), rMax2 

      real*8 d1qv(3,maxCoo/3), d2qv(3,3,maxCoo/3), d3qv(3,3,3,maxCoo/3)
      real*8 d4qv(3,3,3,3,maxCoo/3), d5qv(3,3,3,3,3,maxCoo/3)

      real*8 d1d(3), d2d(3,3), d3d(3,3,3)
      real*8 d4d(3,3,3,3), d5d(3,3,3,3,3) 

      real*8 d1a(3), d2a(3,3), d3a(3,3,3)
      real*8 d4a(3,3,3,3), d5a(3,3,3,3,3) 

      real*8 d(3), q(3,3), o(3,3,3), h(3,3,3,3), fsf(3,maxCoo/3)

      integer i, j, k, l, s, n, m, ii, nx, ny, nz
      integer in2(2), in3(3), in4(4), in5(5)
      real*8 re(3), dr(3), r1, r2, swFunc, dSdr
      logical*1 iSlab

      do n = 1, nM
         do i = 1, 3
            d1v(i,n) = 0.d0
            d1qv(i,n) = 0.d0
            fsf(i,n) = 0.d0
            do j = i, 3
               d2v(i,j,n) = 0.d0
               d2qv(i,j,n) = 0.d0
               do k = j, 3
                  d3v(i,j,k,n) = 0.d0
                  d3qv(i,j,k,n) = 0.d0
                  do l = k, 3
                     d4v(i,j,k,l,n) = 0.d0
                     d4qv(i,j,k,l,n) = 0.d0
                     do s = l, 3
                        d5v(i,j,k,l,s,n) = 0.d0
                        d5qv(i,j,k,l,s,n) = 0.d0
                     end do
                  end do
               end do
            end do
         end do
      end do

      NCz = NC
      if (iSlab) NCz = 0

      do n = 1, nM
         do m = 1, nM

            do nx = -NC, NC
               re(1) = a(1) * nx
               do ny = -NC, NC
                  re(2) = a(2) * ny
                  do nz = -NCz, NCz
                     re(3) = a(3) * nz

                     if ( (n.eq.m) .and. (nx.eq.0) .and. (ny.eq.0) .and.
     $                    (nz.eq.0)) goto 11

                     do i = 1, 3
                        dr(i) = rCM(i,n) - rCM(i,m)
                        if (dr(i) .gt. a2(i)) then
                           dr(i) = dr(i) - a(i)
                        else if (dr(i) .lt. -a2(i)) then
                           dr(i) = dr(i) + a(i)
                        end if
                        dr(i) = dr(i) + re(i)
                     end do

                     r2 = dr(1)**2 + dr(2)**2 + dr(3)**2

                     if (r2 .gt. rMax2) goto 11
                     r1 = sqrt(r2)
                     call SFdsf(r1, swFunc, dSdr)


                     do i = 1, 3
                        d(i) = dpole(i,m) - dpole0(i,m)
                     end do
                     call dDpole(d, dr, d1a, d2a, d3a, d4a, d5a)

                     do j = 1, 3
                        do i = 1, 3
                           q(i,j) = qpole(i,j,m) - qpole0(i,j,m)
                        end do
                     end do
                     call dQpole(q, dr, d1d, d2d, d3d, d4d, d5d)
                     call addDerivA(d1a, d2a, d3a, d4a, d5a, d1d, d2d,
     $                    d3d, d4d, d5d)
c                    d1a ... contains now both induced dip and quad
c                    d1d is still the quad only
                     call addDeriv(d1v, d2v, d3v, d4v, d5v, d1a, d2a,
     $                    d3a, d4a, d5a, n, swFunc)
c                    d1v ... now contains both
                     call addDeriv(d1qv, d2qv, d3qv, d4qv, d5qv, 
     $                    d1d, d2d, d3d, d4d, d5d, n, swFunc)
c                    d1qv ... now contains quad part only
c Why all this back and forth crap ? 
 11               end do
               end do
            end do
         end do
      end do
c For some reason, probably a really silly one
c this is necessary(?)
      do i = 1, 3
         do j = 1, 3
            in2(1) = i
            in2(2) = j
            call insertIN(in2, 2)

            do n = 1, nM
               d2v(i,j,n) = d2v(in2(1), in2(2), n)
               d2qv(i,j,n) = d2qv(in2(1), in2(2), n)
            end do

            do k = 1, 3
               do ii = 1, 2
                  in3(ii) = in2(ii)
               end do
               in3(3) = k
               call insertIN(in3, 3)

               do n = 1, nM
                  d3v(i,j,k,n) = d3v(in3(1),in3(2),in3(3),n)
                  d3qv(i,j,k,n) = d3qv(in3(1),in3(2),in3(3),n)
               end do

               do l = 1, 3
                  do ii = 1, 3
                     in4(ii) = in3(ii)
                  end do
                  in4(4) = l
                  call insertIN(in4, 4)

                  do n = 1, nM
                     d4v(i,j,k,l,n) = d4v(in4(1),in4(2),in4(3),in4(4),n)
                     d4qv(i,j,k,l,n) = d4qv(in4(1),in4(2),in4(3),in4(4)
     $                                      ,n)
                  end do

                  do m = 1, 3
                     do ii = 1, 4
                        in5(ii) = in4(ii)
                     end do
                     in5(5) = m
                     call insertIN(in5, 5)

                     do n = 1, nM
                        d5v(i,j,k,l,m,n) = d5v(in5(1),in5(2),in5(3)
     $                       ,in5(4),in5(5),n)
                        d5qv(i,j,k,l,m,n) = d5qv(in5(1),in5(2),in5(3)
     $                       ,in5(4),in5(5),n)
                     end do

                  end do
               end do
            end do
         end do
      end do
      return
      end


c----------------------------------------------------------------------+
c     Calculate derivatives of the electric field.                     |
c----------------------------------------------------------------------+
      subroutine calcDv(rCM, dpole, qpole, opole, hpole, nM, NC, a, a2,
     $     d1v, d2v, d3v, d4v, d5v, rMax2, fsf, iSlab)

      implicit none
      include '../commonblks/parameters.cmn'
      integer nM, NC, NCz
      real*8 rCM(3,maxCoo/3), a(3), a2(3)
      real*8 dpole(3,maxCoo/3), qpole(3,3,maxCoo/3)
      real*8 opole(3,3,3,maxCoo/3), hpole(3,3,3,3,maxCoo/3)

      real*8 d1v(3,maxCoo/3), d2v(3,3,maxCoo/3), d3v(3,3,3,maxCoo/3)
      real*8 d4v(3,3,3,3,maxCoo/3), d5v(3,3,3,3,3,maxCoo/3), rMax2 

      real*8 d1d(3), d2d(3,3), d3d(3,3,3)
      real*8 d4d(3,3,3,3), d5d(3,3,3,3,3) 

      real*8 d1a(3), d2a(3,3), d3a(3,3,3)
      real*8 d4a(3,3,3,3), d5a(3,3,3,3,3) 

      real*8 d(3), q(3,3), o(3,3,3), h(3,3,3,3), fsf(3,maxCoo/3)

      integer i, j, k, l, s, n, m, ii, nx, ny, nz
      integer in2(2), in3(3), in4(4), in5(5)
      real*8 re(3), dr(3), r1, r2, swFunc, dSdr
      logical*1 iSlab

ctimming
      integer*8 ti, tf, irtc
      real*8 t1, t2, t3, t4, t5, t6, t7
ctimming

      do n = 1, nM
         do i = 1, 3
            d1v(i,n) = 0.d0
            fsf(i,n) = 0.d0
            do j = i, 3                           
               d2v(i,j,n) = 0.d0
               do k = j, 3                              
                  d3v(i,j,k,n) = 0.d0
                  do l = k, 3
                     d4v(i,j,k,l,n) = 0.d0
                     do s = l, 3
                        d5v(i,j,k,l,s,n) = 0.d0
                     end do
                  end do
               end do
            end do
         end do
      end do

      NCz = NC
      if (iSlab) NCz = 0

      do n = 1, nM
         do m = 1, nM
            
            do nx = -NC, NC
               re(1) = a(1) * nx
               do ny = -NC, NC
                  re(2) = a(2) * ny
                  do nz = -NCz, NCz
                     re(3) = a(3) * nz
                  
                     if ( (n.eq.m) .and. (nx.eq.0) .and. (ny.eq.0) .and.
     $                    (nz.eq.0)) goto 11

                     do i = 1, 3
                        dr(i) = rCM(i,n) - rCM(i,m)
                        if (dr(i) .gt. a2(i)) then
                           dr(i) = dr(i) - a(i)
                        else if (dr(i) .lt. -a2(i)) then
                           dr(i) = dr(i) + a(i)
                        end if
                        dr(i) = dr(i) + re(i)
                     end do

                     r2 = dr(1)**2 + dr(2)**2 + dr(3)**2 

                     if (r2 .gt. rMax2) goto 11
                     r1 = sqrt(r2)
                     call SFdsf(r1, swFunc, dSdr)
                     

                     do i = 1, 3
                        d(i) = dpole(i,m)
                     end do
                     call dDpole(d, dr, d1a, d2a, d3a, d4a, d5a)

                     do j = 1, 3
                        do i = 1, 3
                           q(i,j) = qpole(i,j,m)
                        end do
                     end do
                     call dQpole(q, dr, d1d, d2d, d3d, d4d, d5d)
                     call addDerivA(d1a, d2a, d3a, d4a, d5a, d1d, d2d,
     $                    d3d, d4d, d5d)

                     do k = 1, 3
                        do j = 1, 3
                           do i = 1, 3
                              o(i,j,k) = opole(i,j,k,m)
                           end do
                        end do
                     end do
                     call dOpole(o, dr, d1d, d2d, d3d, d4d, d5d)
                     call addDerivA(d1a, d2a, d3a, d4a, d5a, d1d, d2d,
     $                    d3d, d4d, d5d)

                     do l = 1, 3
                        do k = 1, 3
                           do j = 1, 3
                              do i = 1, 3
                                 h(i,j,k,l) = hpole(i,j,k,l,m)
                              end do
                           end do
                        end do
                     end do
                     call dHpole(h, dr, d1d, d2d, d3d, d4d, d5d)
                     call addDerivA(d1a, d2a, d3a, d4a, d5a, d1d, d2d,
     $                    d3d, d4d, d5d)
                     call addDeriv(d1v, d2v, d3v, d4v, d5v, d1a, d2a,
     $                    d3a, d4a, d5a, n, swFunc)

                     call addSwitchingForce(d1a, d2a, d3a, d4a, n,
     $                    dSdr, dr, r1, dpole, qpole, opole, hpole, fsf
     $                    )

 11               end do
               end do
            end do
         end do
      end do

c     Copy all the permutations. (Is this really necessary??)
      do i = 1, 3
         do j = 1, 3
            in2(1) = i
            in2(2) = j
            call insertIN(in2, 2)
            
            do n = 1, nM
               d2v(i,j,n) = d2v(in2(1), in2(2), n)
            end do

            do k = 1, 3
               do ii = 1, 2
                  in3(ii) = in2(ii)
               end do
               in3(3) = k
               call insertIN(in3, 3)

               do n = 1, nM
                  d3v(i,j,k,n) = d3v(in3(1),in3(2),in3(3),n)
               end do

               do l = 1, 3
                  do ii = 1, 3
                     in4(ii) = in3(ii)
                  end do
                  in4(4) = l
                  call insertIN(in4, 4)

                  do n = 1, nM
                     d4v(i,j,k,l,n) = d4v(in4(1),in4(2),in4(3),in4(4),n)
                  end do
                     
                  do m = 1, 3
                     do ii = 1, 4
                        in5(ii) = in4(ii)
                     end do
                     in5(5) = m
                     call insertIN(in5, 5)
                     
                     do n = 1, nM
                        d5v(i,j,k,l,m,n) = d5v(in5(1),in5(2),in5(3)
     $                       ,in5(4),in5(5),n)
                     end do

                  end do
               end do
            end do
         end do
      end do
c$$$      tf = irtc()
c$$$      t4 = (tf-ti) * 1e-9
c$$$      print '(A,f15.6)', 'Permutations: ', t4
c$$$      stop

      return 
      end
c-----------------------------------------------------------------------
      subroutine insertIN(i, n)

      implicit none
      integer i(*), j, k, iaux, n

      if (n .ge. 2) then
         iaux = i(n)
         j = n-1
         do while (i(j) .gt. iaux .and. j .ge. 1)
            i(j+1) = i(j)
            j = j - 1
         end do
         i(j+1) = iaux
      end if

      return
      end
c-----------------------------------------------------------------------
      function delta(i, j)
      implicit none
      integer i, j, delta

      if (i.eq.j) then
         delta = 1
      else
         delta = 0
      end if

      return
      end
c-----------------------------------------------------------------------
      subroutine addQMFields(d1v, d2v, d3v, d4v, d5v, eQM, deQM,
     $     ddeQM, dddeQM, ddddeQM, nM)

      implicit none
      include '../commonblks/parameters.cmn'

      real*8 d1v(3,maxCoo/3), d2v(3,3,maxCoo/3), d3v(3,3,3,maxCoo/3)
      real*8 d4v(3,3,3,3,maxCoo/3), d5v(3,3,3,3,3,maxCoo/3)

      real*8 eQM(3,maxCoo/3), deQM(3,3,maxCoo/3), ddeQM(3,3,3,maxCoo/3)
      real*8 dddeQM(3,3,3,3,maxCoo/3), ddddeQM(3,3,3,3,3,maxCoo/3)

      integer nM, n, i, j, k, l, s

      do n = 1, nM
         do i = 1, 3
            d1v(i,n) = d1v(i,n) - eQM(i,n)
            do j = 1, 3
               d2v(i,j,n) = d2v(i,j,n) - deQM(i,j,n)
               do k = 1, 3
                  d3v(i,j,k,n) = d3v(i,j,k,n) - ddeQM(i,j,k,n)
                  do l = 1, 3
                     d4v(i,j,k,l,n) = d4v(i,j,k,l,n) - dddeQM(i,j,k,l,n)
                     do s = 1, 3
                        d5v(i,j,k,l,s,n) = d5v(i,j,k,l,s,n) - ddddeQM(i,
     $                        j,k,l,s,n)
                     end do
                  end do
               end do
            end do
         end do
      end do

      return
      end

c-----------------------------------------------------------------------

      subroutine addQMFieldsToZeros(d1q, d2q, d3q, d4q, d5q, eQM, deQM,
     $     ddeQM, dddeQM, ddddeQM, nM)

      implicit none
      include '../commonblks/parameters.cmn'

      real*8 d1q(3,maxCoo/3), d2q(3,3,maxCoo/3), d3q(3,3,3,maxCoo/3)
      real*8 d4q(3,3,3,3,maxCoo/3), d5q(3,3,3,3,3,maxCoo/3)

      real*8 eQM(3,maxCoo/3), deQM(3,3,maxCoo/3), ddeQM(3,3,3,maxCoo/3)
      real*8 dddeQM(3,3,3,3,maxCoo/3), ddddeQM(3,3,3,3,3,maxCoo/3)

      integer nM, n, i, j, k, l, s

      do n = 1, nM
         do i = 1, 3
            d1q(i,n) = 0.d0
            do j = i, 3
               d2q(i,j,n) = 0.d0
               do k = j, 3
                  d3q(i,j,k,n) = 0.d0
                  do l = k, 3
                     d4q(i,j,k,l,n) = 0.d0
                     do s = l, 3
                        d5q(i,j,k,l,s,n) = 0.d0
                     end do
                  end do
               end do
            end do
         end do
      end do

      do n = 1, nM
         do i = 1, 3
            d1q(i,n) = d1q(i,n) - eQM(i,n)
            do j = 1, 3
               d2q(i,j,n) = d2q(i,j,n) - deQM(i,j,n)
               do k = 1, 3
                  d3q(i,j,k,n) = d3q(i,j,k,n) - ddeQM(i,j,k,n)
                  do l = 1, 3
                     d4q(i,j,k,l,n) = d4q(i,j,k,l,n) - dddeQM(i,j,k,l,n)
                     do s = 1, 3
                        d5q(i,j,k,l,s,n) = d5q(i,j,k,l,s,n) - ddddeQM(i,
     $                        j,k,l,s,n)
                     end do
                  end do
               end do
            end do
         end do
      end do

      return
      end

c-----------------------------------------------------------------------
      subroutine addDeriv(d1v, d2v, d3v, d4v, d5v, d1d, d2d, d3d, d4d,
     $     d5d, n, swFunc) 

      implicit none
      include '../commonblks/parameters.cmn'

      real*8 d1v(3,maxCoo/3), d2v(3,3,maxCoo/3), d3v(3,3,3,maxCoo/3)
      real*8 d4v(3,3,3,3,maxCoo/3), d5v(3,3,3,3,3,maxCoo/3) 

      real*8 d1d(3), d2d(3,3), d3d(3,3,3)
      real*8 d4d(3,3,3,3), d5d(3,3,3,3,3) 
      real*8 swFunc
      
      integer n, i, j, k, l, s

      do i = 1, 3
         d1v(i,n) = d1v(i,n) + d1d(i) * swFunc
         do j = i, 3                           
            d2v(i,j,n) = d2v(i,j,n) + d2d(i,j) * swFunc
            do k = j, 3                              
               d3v(i,j,k,n) = d3v(i,j,k,n) + d3d(i,j,k) * swFunc
               do l = k, 3
                  d4v(i,j,k,l,n) = d4v(i,j,k,l,n) + d4d(i,j,k,l)
     $                 * swFunc
                  do s = l, 3
                     d5v(i,j,k,l,s,n) = d5v(i,j,k,l,s,n) + d5d(i,j,k,l,s
     $                    ) * swFunc
                  end do
               end do
            end do
         end do
      end do

      return
      end
c-----------------------------------------------------------------------
      subroutine addDerivA(d1a, d2a, d3a, d4a, d5a, d1d, d2d, d3d, d4d,
     $     d5d) 

      implicit none
      include '../commonblks/parameters.cmn'

      real*8 d1a(3), d2a(3,3), d3a(3,3,3)
      real*8 d4a(3,3,3,3), d5a(3,3,3,3,3) 

      real*8 d1d(3), d2d(3,3), d3d(3,3,3)
      real*8 d4d(3,3,3,3), d5d(3,3,3,3,3) 

      integer n, i, j, k, l, s

      do i = 1, 3
         d1a(i) = d1a(i) + d1d(i)
         do j = i, 3                           
            d2a(i,j) = d2a(i,j) + d2d(i,j)
            do k = j, 3                              
               d3a(i,j,k) = d3a(i,j,k) + d3d(i,j,k)
               do l = k, 3
                  d4a(i,j,k,l) = d4a(i,j,k,l) + d4d(i,j,k,l)
                  do s = l, 3
                     d5a(i,j,k,l,s) = d5a(i,j,k,l,s) + d5d(i,j,k,l,s)
                  end do
               end do
            end do
         end do
      end do

      return
      end
c-----------------------------------------------------------------------
      subroutine addSwitchingForce(d1a, d2a, d3a, d4a, n, dSdr, dr,
     $     r1, dpole, qpole, opole, hpole, fsf)

      implicit none
      include '../commonblks/parameters.cmn'

      real*8 d1a(3), d2a(3,3), d3a(3,3,3), d4a(3,3,3,3)
      real*8 dSdr, dr(3), r1, fsf(3,maxCoo/3), u
      real*8 dpole(3,maxCoo/3), qpole(3,3,maxCoo/3)
      real*8 opole(3,3,3,maxCoo/3), hpole(3,3,3,3,maxCoo/3)
      
      integer i, ii, j, k, l, in2(2), in3(3), in4(4), n

c     Copy all the permutations. 
      do i = 1, 3
         do j = 1, 3
            in2(1) = i
            in2(2) = j
            call insertIN(in2, 2)
            
            d2a(i,j) = d2a(in2(1), in2(2))

            do k = 1, 3
               do ii = 1, 2
                  in3(ii) = in2(ii)
               end do
               in3(3) = k
               call insertIN(in3, 3)

               d3a(i,j,k) = d3a(in3(1),in3(2),in3(3))

               do l = 1, 3
                  do ii = 1, 3
                     in4(ii) = in3(ii)
                  end do
                  in4(4) = l
                  call insertIN(in4, 4)

                  d4a(i,j,k,l) = d4a(in4(1),in4(2),in4(3),in4(4))
                     
               end do
            end do
         end do
      end do
      
      u = 0.d0
      do i = 1, 3
         u = u + d1a(i) * dpole(i,n)
         do j = 1, 3
            u = u + d2a(i,j) * qpole(i,j,n) / 3.d0
            do k = 1, 3
               u = u + d3a(i,j,k) * opole(i,j,k,n) / 15.d0
               do l = 1, 3
                  u = u + d4a(i,j,k,l) * hpole(i,j,k,l,n) / 105.d0
               end do
            end do
         end do
      end do

      u = -u * dSdr / r1
      do i = 1, 3
         fsf(i,n) = fsf(i,n) + u * dr(i)
      end do

      return
      end
