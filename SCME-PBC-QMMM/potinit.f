C AA 29-06-2004
C Read in parameters for the potential

      SUBROUTINE potinit()
        IMPLICIT NONE
        INTEGER i,ip,stat
        real*8 rcuttrash(3), rskintrash(3)
        CHARACTER(LEN=80) text

        INCLUDE '../commonblks/potparam.cmn'
        INCLUDE '../commonblks/compotent.cmn'           

        character(len=255) inpdir
        character(len=255) inpfile

        CALL getenv("SCME_PATH", inpdir)
        inpfile = TRIM(inpdir)//'param.pot'
        OPEN(421,FILE=inpfile,STATUS='old',ACTION='read')
        READ(421,*) (fsti(i),text , i=1,nl)        

        READ(421,'(/)')
c       now provided by ase, reading into gunk variables
c       to get to correct point in input file... 
        READ(421,*) (rcuttrash(i),i=1,3) 
        READ(421,*) (rskintrash(i),i=1,3)
        READ(421,*) indf1

        npotpar=0
        DO ip=1,MAXPOTPAR
           READ(421,*,IOSTAT=stat) (potpar(ip,i),i=1,3)
           IF (stat /= 0) EXIT
           npotpar=npotpar+1
        END DO
c        WRITE(*,*) ' FROM POTINIT ... npotpar = ',npotpar

        CLOSE(421)

c        ASE: moved to main 
c        DO  i=1,3
c          rcut2(i)=rcut(i)**2
c          rskin2(i)=rskin(i)**2
c        END DO

      END 
